DESCRIPTION = "Creates u-boot mkpimage"
SECTION = ""

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://licenses/README;md5=a50d1e8cd5a9412d8d27a547b29ae866"

SRC_URI = "file://licenses/README"

inherit deploy

DEPENDS = "u-boot-dreamchip-arria10som"

# built by u-boot-dreamchip-arria10som, put in deploy/
UBOOT_BINARY = "u-boot-dtb.bin"

# resulting u-boot-image to be put into deploy/ by this recipe
UBOOT_IMAGE = "uboot_w_dtb-mkpimage.bin"

# path to Alteras mkpimage
MKPIMAGE = "/cad/altera/Quartus/17.1.0.590/embedded/host_tools/altera/mkpimage/mkpimage"

do_deploy[nostamp] = "1"

do_deploy() {
    ${MKPIMAGE} --header-version 1 -o ${DEPLOYDIR}/${UBOOT_IMAGE} \
        ${DEPLOY_DIR_IMAGE}/${UBOOT_BINARY} \
        ${DEPLOY_DIR_IMAGE}/${UBOOT_BINARY} \
        ${DEPLOY_DIR_IMAGE}/${UBOOT_BINARY} \
        ${DEPLOY_DIR_IMAGE}/${UBOOT_BINARY}
}

addtask do_deploy after do_compile

S = "${WORKDIR}"

COMPATIBLE = "dreamchip-arria10som"
