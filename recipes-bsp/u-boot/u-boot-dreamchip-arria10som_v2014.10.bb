require ${COREBASE}/meta/recipes-bsp/u-boot/u-boot.inc
PR="r2"

SRCREV = "632994aefb4881659abfeb59b5e11fd15c802203"

UBOOT_LOCALVERSION = "-${@bb.fetch2.get_srcrev(d)[8:15]}"

UBOOT_LOCALVERSION = "-${@bb.fetch2.get_srcrev(d)[8:15]}"

UBOOT_BRANCH ?= "dct-socfpga-${PV}-arria10-q17.1"
UBOOT_REPO ?= "git://gitlab.com/dreamchip/arria10som-uboot.git"
UBOOT_PROT ?= "https"

SRC_URI  = "${UBOOT_REPO};protocol=${UBOOT_PROT};user=${UBOOT_USER};branch=${UBOOT_BRANCH}"
SRC_URI += "file://fix-build-error-under-gcc6.patch"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=c7383a594871c03da76b3707929d2919"

DEPENDS += "dtc-native"

# TODO(ahlers) usually this should be defined in machine files
UBOOT_CONFIG ?= "dreamchip-arria10som"
UBOOT_CONFIG[dreamchip-arria10som]="dreamchip_arria10som_sdmmc_defconfig"

UBOOT_BINARY = "u-boot-dtb.bin"

