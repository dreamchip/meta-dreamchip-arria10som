DEFAULTTUNE = "cortexa9hf-neon"


PREFERRED_PROVIDER_virtual/kernel = "linux-dreamchip-arria10som-ltsi"
PREFERRED_VERSION_linux-dreamchip-arria10som = "4.9.78%"

PREFERRED_PROVIDER_virtual/bootloader = "u-boot-dreamchip-arria10som"
PREFERRED_VERSION_u-boot-socfpga = "v2014.10%"

KMACHINE = "arria10"
DEFAULTTUNE = "cortexa9hf-neon"


IMAGE_FSTYPES = "ext4 ext4.gz tar.gz"

#MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS += " u-boot-fw-utils"
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS += " kernel-modules"


# 'backfill_considered' *disables* certain features
MACHINE_FEATURES_BACKFILL_CONSIDERED += " rtc"

