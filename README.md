# meta-dreamchip-arria10som

This is the **[Dream Chip](http://www.dreamchip.de "Dream Chip Technologies GmbH") meta-dreamchip-arria10som** repository.

It contains a meta layer with [Yocto](https://www.yoctoproject.org/ "The Yocto Project") recipies for the
**[Dream Chip Arria10 SoM](https://www.dreamchip.de/products/arria-10-system-on-module.html "The Arria 10 System on Module in detail")**.


Please see the corresponding sections below for details.
## Table of Contents

1. [Dependencies](#dependencies)
2. [Patches](#patches)
3. [Adding the dreamchip-arria10som layer to your build](#adding)  
4. [Start with a Dream Chip Arria10 SoM Yocto build](#start_with_dreamchip_yocto)
5. [Misc](#misc)


## 1. Dependencies <a name="dependencies"></a>

This layer depends on:

  URI: git://git.openembedded.org/bitbake
  branch: morty

  URI: git://git.openembedded.org/openembedded-core
  layers: meta
  branch: morty

  URI: git://git.linaro.org/open-embedded
  layers: meta-linaro
  branch: morty

  URI: git://github.com/kraj
  layers: meta-altera
  branch: master

## 2. Patches <a name="patches"></a>

Please submit any patches against the dreamchip-arria10som layer to the
the maintainer:

Dream Chip Technologies GmbH <opensource@dreamchip.de>


## 3. Adding the dreamchip-arria10som layer to your build <a name="adding"></a>

In order to use this layer, you need to make the build system aware of
it.

Assuming the dreamchip-arria10som layer exists at the top-level of your
yocto build tree, you can add it to the build system by adding the
location of the dreamchip-arria10som layer to bblayers.conf, along with any
other layers needed. e.g.:

	BBLAYERS ?= " \
		/path/to/yocto/meta \
		/path/to/yocto/meta-poky \
		/path/to/yocto/meta-yocto-bsp \
		/path/to/yocto/meta-linaro \
		/path/to/yocto/meta-altera \
		/path/to/yocto/meta-dreamchip-arria10som \
		"

## 4. Start with a Dream Chip Arria10 SoM Yocto build <a name="start_with_dreamchip_yocto"></a>

**Dream Chip** is supporting a **Yocto/Poky** build which is using **Google's** repo tool to setup the project structure.  
The starting point(manifest file) is located at the [Dream Chip arria10som-manifest](https://gitlab.com/dreamchip/arria10som-manifest) repository.

## 5. Misc <a name="misc"></a>

Please note: The u-boot recipe build a u-boot image with ***SD-Card*** support by default.  
If you need a uboot with ***QSPI + eMMC*** support please adapte the recipe or build the u-boot separately 
from the [Dream Chip arria10som-uboot](https://gitlab.com/dreamchip/arria10som-uboot) repository.
