# based on alteras kernel recipes from meta-altera
DESCRIPTION = "Dreamchip Arria10 SoM Linux kernel"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://${S}/COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

KERNEL_REPO ?= "git://gitlab.com/dreamchip/arria10som-linux.git"
KERNEL_PROT ?= "https"

# Kernel CONFIG_LOCALVERSION
LINUX_VERSION_EXTENSION ?= "-dreamchip-arria10som-${@bb.fetch2.get_srcrev(d)[8:17]}"

# Build branch name
LINUX_VERSION_PREFIX ?= "dct-socfpga-"
LINUX_VERSION_SUFFIX ?= "-ltsi"
LINUX_VERSION ?= "4.9.78"

SRCREV ?= "${AUTOREV}"
SRCREV_machine ?= "${AUTOREV}"
PV = "${LINUX_VERSION}${LINUX_VERSION_SUFFIX}"
PV_append = "+git${SRCPV}"


KBRANCH ?= "${LINUX_VERSION_PREFIX}${LINUX_VERSION}${LINUX_VERSION_SUFFIX}"

SRC_URI = "${KERNEL_REPO};protocol=${KERNEL_PROT};user=${KERNEL_USER};branch=${KBRANCH}"

# Default kernel devicetrees
KERNEL_DEVICETREE_arria10 ?= "dreamchip_arria10som_sdmmc.dtb dreamchip_arria10som_qspi.dtb"

require recipes-kernel/linux/linux-yocto.inc
require linux-dreamchip-arria10som-configs.inc
