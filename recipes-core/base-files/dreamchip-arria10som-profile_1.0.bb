SUMMARY = "Additional aliases for the shared /etc/profile.d/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://dreamchip-arria10som-alias.sh \
"

S = "${WORKDIR}"

do_install () {
	install -d ${D}${sysconfdir}/profile.d
	install -m 0755 ${WORKDIR}/dreamchip-arria10som-alias.sh ${D}${sysconfdir}/profile.d
}

