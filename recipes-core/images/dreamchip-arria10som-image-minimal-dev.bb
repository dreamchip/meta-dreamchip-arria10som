require recipes-core/images/core-image-minimal-dev.bb

DESCRIPTION = "The Dreamchip Arria10 SoM minimal image, suitable for development work."

EXTRA_IMAGE_FEATURES += " debug-tweaks"
EXTRA_IMAGE_FEATURES += " tools-debug"

include dreamchip-arria10som-software.inc

