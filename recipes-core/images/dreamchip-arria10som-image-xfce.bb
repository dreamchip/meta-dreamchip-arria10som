DESCRIPTION = "A XFCE dreamchip image."

IMAGE_INSTALL = "packagegroup-core-boot \
    ${ROOTFS_PKGMANAGE_BOOTSTRAP} \
    packagegroup-core-x11 \
    packagegroup-xfce-dct \
    kernel-modules \
"
# additional image features
IMAGE_FEATURES += "ssh-server-dropbear"

REQUIRED_DISTRO_FEATURES = "x11"

IMAGE_LINGUAS ?= " "

LICENSE = "MIT"

export IMAGE_BASENAME = "dreamchip-arria10som-image-xfce"

inherit core-image
inherit extrausers
EXTRA_USERS_PARAMS = "usermod -P dreamchip root;"