require recipes-core/images/core-image-minimal.bb

DESCRIPTION = "The Dreamchip Arria10 SoM minimal image."

inherit extrausers
EXTRA_USERS_PARAMS = "usermod -P dreamchip root;"

include dreamchip-arria10som-software.inc