# software to be installed into the resulting images
IMAGE_INSTALL += "devmem2"
IMAGE_INSTALL += "i2c-tools"
IMAGE_INSTALL += "kernel-devicetree"
IMAGE_INSTALL += "e2fsprogs"
IMAGE_INSTALL += "e2fsprogs-resize2fs"
IMAGE_INSTALL += "e2fsprogs-tune2fs"

# additional image features
IMAGE_FEATURES += "ssh-server-dropbear"
IMAGE_FEATURES += "read-only-rootfs"
